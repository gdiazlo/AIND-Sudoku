"""
Udacity AIND First project solution
Sudoku solver
Strategies implemented
  - elimination
  - one choice
  - naked twins
  - deph-first search
"""

assignments = []

ROWS = 'ABCDEFGHI'
COLS = '123456789'

def cross(lista, listb):
    "Cross product of elements in list A and elements in list B."
    return [a+b for a in lista for b in listb]

BOXES = cross(ROWS, COLS)

ROW_UNITS = [cross(r, COLS) for r in ROWS]
COLUMN_UNITS = [cross(ROWS, c) for c in COLS]
SQUARE_UNITS = [cross(rs, cs) for rs in ('ABC', 'DEF', 'GHI') for cs in ('123', '456', '789')]

DIAGONAL1 = [a[0]+a[1] for a in zip(ROWS, COLS)]
DIAGONAL2 = [a[0]+a[1] for a in zip(ROWS, COLS[::-1])]
DIAGONAL_UNITS = [DIAGONAL1, DIAGONAL2]
UNITLIST = ROW_UNITS + COLUMN_UNITS + SQUARE_UNITS + DIAGONAL_UNITS

UNITS = dict((box, [unit for unit in UNITLIST if box in unit]) for box in BOXES)
PEERS = dict((box, set(sum(UNITS[box], []))-set([box])) for box in BOXES)

def assign_value(values, box, value):
    """
    Please use this function to update your values dictionary!
    Assigns a value to a given box. If it updates the board record it.
    """

    # Don't waste memory appending actions that don't actually change any values
    if values[box] == value:
        return values

    values[box] = value
    if len(value) == 1:
        assignments.append(values.copy())
    return values

def naked_twins(values):
    """Eliminate values using the naked twins strategy.

    For each unit, select all boxes which have two possible values, and from those
    select the ones which both values are the same. Then, remove those values from the rest
    of the unit elements, and process next unit. This will remove the naked twins
    values from all of their shared peers, either by square, column, row or diagonal, as they'll
    be detected on each of those units.

    Input: values(dict): a dictionary of the form {'box_name': '123456789', ...}

    Output: the values dictionary with the naked twins eliminated from peers.

    """
    # For all units, try to find its naked twins and eliminate options from their peers
    for unit in UNITLIST:
        # select all elements with two posibble values
        twins = {}
        for box in unit:
            # we assume values of boxes are always ordered from 1 to 9.
            if len(values[box]) == 2:
                twins.setdefault(values[box], []).append(box)

        # naked twins are all the pairs of boxes found in the same unit with
        # the same possible set of two values
        naked_twins_list = [boxlist for val, boxlist in twins.items() if len(boxlist) == 2]

        # eliminate the naked twins as possibilities for their peers
        for twina, twinb in naked_twins_list:
            vals = values[twina]
            for box in unit:
                if box != twina and box != twinb and len(values[box]) > 1:
                    for val in vals:
                        assign_value(values, box, values[box].replace(val, ""))

    return values

def grid_values(grid):
    """
    Convert grid into a dict of {square: char} with '123456789' for empties.

    Input: A grid in string form.

    Output: A grid in dictionary form
            Keys: The boxes, e.g., 'A1'
            Values: The value in each box, e.g., '8'. If the box has no value,
                    then the value will be '123456789'.

    """

    if len(grid) != 81:
        return None

    chars = []
    digits = '123456789'
    for char in grid:
        if char in digits:
            chars.append(char)
        if char == '.':
            chars.append(digits)


    values = dict(zip(BOXES, ['']*81))
    for i, box in enumerate(BOXES):
        assign_value(values, box, chars[i])

    return values

def display(values):
    """
    Display the values as a 2-D grid.

    Input: The sudoku in dictionary form

    Output: None

    """
    if values is None:
        print('Unable to solve this puzzle!')
        return

    width = 1+max(len(values[box]) for box in BOXES)
    line = '+'.join(['-'*(width*3)]*3)
    for row in ROWS:
        print(''.join(values[row+col].center(width)+('|' if col in '36' else '')
                      for col in COLS))
        if row in 'CF':
            print(line)
    return

def eliminate(values):
    """
    Go through all the boxes, and whenever there is a box with a value,
    eliminate this value from the values of all its peers.

    Input: A sudoku in dictionary form.

    Output: The resulting sudoku in dictionary form.

    """
    solved_values = [box for box, val in values.items() if len(val) == 1]
    for box in solved_values:
        digit = values[box]
        for peer in PEERS[box]:
            assign_value(values, peer, values[peer].replace(digit, ''))

    return values

def only_choice(values):
    """
    Go through all the units, and whenever there is a unit with a value that
    only fits in one box, assign the value to this box.

    Input: A sudoku in dictionary form.

    Output: The resulting sudoku in dictionary form.

    """
    for unit in UNITLIST:
        for digit in '123456789':
            dplaces = [box for box in unit if digit in values[box]]
            if len(dplaces) == 1:
                assign_value(values, dplaces[0], digit)
    return values

def reduce_puzzle(values):
    """
    Iterate eliminate(),only_choice() and naked_twins(). If at some point,
    there is a box with no available values, return None.
    If the sudoku is solved, return the sudoku.
    If after an iteration of both functions, the sudoku remains the same, return the sudoku.

    Input: A sudoku in dictionary form.

    Output: The resulting sudoku in dictionary form.

    """
    if values is None:
        return None

    stalled = False
    while not stalled:
        solved_values_before = len([box for box, val in values.items() if len(val) == 1])
        values = eliminate(values)
        values = only_choice(values)
        values = naked_twins(values)
        solved_values_after = len([box for box, val in values.items() if len(val) == 1])
        stalled = solved_values_before == solved_values_after
        if len([box for box, val in values.items() if len(val) == 0]):
            return None
    return values

def search(values):
    """
    Using depth-first search and propagation, try all possible values.

    Input: A sudoku in dictionary form.

    Output: The resulting sudoku in dictionary form solved or False.

    """
    # First, reduce the puzzle using the previous function
    values = reduce_puzzle(values)
    if values is None:
        return None ## Failed earlier
    if all(len(values[box]) == 1 for box in BOXES):
        return values ## Solved!
    # Choose one of the unfilled squares with the fewest possibilities
    box = min(box for box in BOXES if len(values[box]) > 1)
    # Now use recurrence to solve each one of the resulting sudokus, and
    for value in values[box]:
        new_sudoku = values.copy()
        new_sudoku[box] = value
        attempt = search(new_sudoku)
        if attempt:
            return attempt

def solve(grid):
    """
    Find the solution to a Sudoku grid.

    Args:
        grid(string): a string representing a sudoku grid.
            Example: '2.............62....1....7...6..8...3...9...
                      7...6..4...4....8....52.............3'
    Returns:
        The dictionary representation of the final sudoku grid. False if no solution exists.

    """
    return search(grid_values(grid))

if __name__ == '__main__':
    DIAG_SUDOKU_GRID = '2.............62....1....7...6..8...3...9...7...6..4...4....8....52.............3'
    display(solve(DIAG_SUDOKU_GRID))

    try:
        from visualize import visualize_assignments
        visualize_assignments(assignments)

    except SystemExit:
        pass
    except ModuleNotFoundError:
        print('We could not visualize your board due to a pygame issue.'+
              'Not a problem! It is not a requirement.')
